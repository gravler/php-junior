<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\CreateSecretType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"get", "post"})
     */
    public function index(Request $request, ValidatorInterface $validator): Response
    {
        $form = $this->createForm(CreateSecretType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $res = $form->getData();

            $message = new Message();
            $message->setText($res['secret_text']);
            $message->setPassword(password_hash($res['passphrase'], PASSWORD_DEFAULT));

            $errors = $validator->validate($message);

            if (count($errors) > 0) {
                return new Response((string) $errors, 400);
            }

            $entityManager->persist($message);
            $entityManager->flush();

            return $this->render('default/index.html.twig', [
                'messageId' => $message->getId()
            ]);
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
