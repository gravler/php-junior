<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\OpenSecretType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecretController extends AbstractController
{
    /**
     * @Route("/secret/{id}", name="secret")
     */
    public function index(Request $request, $id): Response
    {
        $form = $this->createForm(OpenSecretType::class);

        $form->handleRequest($request);

        $message = $this->getDoctrine()
            ->getRepository(Message::class)
            ->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Secret not found');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $res = $form->getData();
            $hash = $message->getPassword();
            $text = $message->getText();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($message);
            $entityManager->flush();

            if(password_verify($res['passphrase'], $hash)) {
                return $this->render('secret/index.html.twig', [
                    'text' => $text,
                ]);
            } else {
                return $this->render('secret/index.html.twig', [
                    'invalidPassword' => true,
                ]);
            }
        }

        return $this->render('secret/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
